<?php

require_once 'helpers/RandomArr.php';
require_once 'helpers/FindNearElement.php';

$min = -1000; $max = 1000;
$length = 10;

$randIntVal = random_int($min, $max);

try {
    $rand = new \RandomArr();
    $rand->rand($length, $min, $max);
    $randomArray = $rand->getRandomArray();
    sort($randomArray);

    $temp = new \FindNearElement();
    echo $temp->getNearArraySortedElement($randIntVal, $randomArray);
}catch (Exception $error){
    echo $error->getMessage();
}


