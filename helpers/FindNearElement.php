<?php

Class FindNearElement{

    public function getNearArraySortedElement(int $element, array $array){

        if (empty($array)) {
            throw new \Exception('The array is empty');
        }

        $minItem = [];
        foreach ($array as $key => $value){
            $value <= $element ? $minItem = ['KEY' => $key,'VALUE' => $value] : '';
        }

        if (empty($minItem)){//если не было найдено числа меньше чем искомое, значит возвращаем первый элемент массива
            return array_pop(array_reverse($array));
        }
        else if (isset($array[$minItem['KEY'] + 1])) {//если элемент не последний в массиве тогда сравниваем соседние
            return ($element - $array[$minItem['KEY']]) < ($array[$minItem['KEY'] + 1] - $element) ? $array[$minItem['KEY']] : $array[$minItem['KEY'] + 1];
        }
        else return $minItem['VALUE'];

    }

}
