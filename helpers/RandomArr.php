<?php

class RandomArr{

    private $random_array = array();

    public function rand (int $length, int $min, int $max){
        if (!empty($this->random_array)){
            throw new \Exception('The array is not empty');
        }
        for ($i = 0; $i < $length; $i++){
            $this->random_array[$i] = random_int($min, $max);
        }
    }

    public function getRandomArray(){
        return $this->random_array;
    }

}